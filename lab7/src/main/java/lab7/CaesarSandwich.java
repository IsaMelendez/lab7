package lab7;

/**
 * Class CaesarSandwich stores information about a caesar sandwich
 * @author Isa Melendez
 * @date 10/18/2023
 */
public class CaesarSandwich extends VegetarianSandwich{
    /**
     * Constructs a caesar sandwich
     */
    public CaesarSandwich(){
        this.filling = "Caesar Dressing";
    }

    /**
     * Abstract method that returns a string that contains protein
     * @return Protein 
     */
    @Override
    public String getProtein(){
        return "Anchovies";
    }

    /**
     * Evaluates if the sandwich is vegetarain based on its ingredients
     * @return false, because it has anchovies
     */
    @Override 
    public boolean isVegetarian(){
        return false;
    }
}
