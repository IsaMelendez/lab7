package lab7;

/**
*TofuSandwich is a class that haves methods about a Tofu sandwich
*@author Patricia Boujeily
*@date 10/18/2023
*/
public class TofuSandwich extends VegetarianSandwich {

    /**
     * Constructs a TofuSandwich  object
     */
    public TofuSandwich(){
        this.filling = "";
    }

    /**
     * Gets the protein from the sandwich object
     * @return a string Tofu
     */
    public String getProtein(){
        return "Tofu";
    }
}
