//Patricia Bourjeily
package lab7;

/**
*BagelSandwich is a class that haves methods about a Bagel sandwich
*@author Patricia Boujeily
*@date 10/18/2023
*/
public class BagelSandwich implements ISandwich{
    private String filling;

    /**
     * Constructor for the Bagel Class
     * takes no inputs
     */
    public BagelSandwich(){
        this.filling = "";
    }

    /**
     * Method addFilling to add the input 
     * topping to the filling
     */
    public void addFilling(String topping){
        this.filling += " "+ topping;
    }

    /**
     * Getter to get the field filling
     * @return this.filling
     */
    public String getFilling(){
        return this.filling;
    }

    /**
     * Method isVegetarian that will trow an 
     * UnsupportedOperationException exception
     * @return boolean
     */
    public boolean isVegetarian(){
        throw new UnsupportedOperationException("Is Vegetarian error");
    }
    
}
