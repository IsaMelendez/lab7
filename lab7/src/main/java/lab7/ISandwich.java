package lab7;

/**
*ISandwich is a interface that holds methods concerning a sandwich
*@author Patricia Bourjeily
*@date 10/18/2023
*/
public interface ISandwich {

    /**
     * Gets the filling from the sandwich object
     */
    public String getFilling();

    /**
     * Method addFilling to add the input 
     * topping to the filling
     */
    public void addFilling(String topping);

    /**
     * Method isVegetarian that will trow an 
     * UnsupportedOperationException exception
     */
    public boolean isVegetarian();
}
