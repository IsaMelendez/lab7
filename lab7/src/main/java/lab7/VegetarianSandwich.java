package lab7;

/**
*VegetarianSandwich is a class that holds information and methods concerning a vegetarian sandwich
*@author Isa Melendez
*@date 10/18/2023
*/

public abstract class VegetarianSandwich implements ISandwich{

    protected String filling;
    private String[] notAllowed = {"chicken", "beef", "fish", "meat", "pork"};

    /**
     * Constructs a vegetarian sandwich object
     */
    public VegetarianSandwich(){
        this.filling = "";
    }

    /**
     * Gets the fillings from the sandwich object
     * @return The fillings
     */
    public String getFilling(){
        return filling;
    }

    /**
     * Adds a new filling to the vegetarian sandwich
     * @param topping The name of the topping we want to add
     */
    public void addFilling(String topping){
        for(int i=0; i<notAllowed.length; i++){
            if(notAllowed[i]==topping){
                throw new IllegalArgumentException("This sandwich should be vegetarian!");
            }
        }
        if(filling.length()==0){
            this.filling = topping;
        }
        else{
            this.filling += ", " + topping;
        }
    }

    /**
     * Evaluates if the sandwich is vegetarian
     * @return Since this class refers to a vegetarian sandwich, always returns true
     */
    public boolean isVegetarian(){
        return true;
    }

    /**
     * Evaluates if the sandwich is vegan based on its ingredients
     * @return True if it is vegan, false if it is not
     */
    public boolean isVegan(){
        if(this.filling.contains("egg")||this.filling.contains("cheese")){
            return false;
        }
        return true;
    }

    /**
     * Abstract method that returns a string that contains protein
     * @return Protein 
     */
    public abstract String getProtein();
}
