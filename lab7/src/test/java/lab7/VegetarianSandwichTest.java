package lab7;

import static org.junit.Assert.*;

import org.junit.Test;

/**
* VegetarianSandwichTest is a class used for testing packages out
* information about a Vegetarian Sandwich
* @author Patricia Bourjeily
* @version 10/11/2023
*/
public class VegetarianSandwichTest 
{

    /**
     * Method to check the constructor Height
     * if value is negative
     */
    @Test
    public void testConstructorCreatedObject()
    {
        VegetarianSandwich firstSand = new VegetarianSandwich();
    }

   /**
     * Method to test the getter getFilling
     * if value is correct
     */
    @Test
    public void testGetFilling()
    {
        VegetarianSandwich firstSandw = new VegetarianSandwich();
        assertEquals("", firstSandw.getFilling());
    }

    /**
     * Method to check the method IsVegetarian
     * if value is true
     */
    @Test
    public void testIsVegetarian()
    {
        VegetarianSandwich firstSandw = new VegetarianSandwich();
        assertTrue(firstSandw.isVegetarian());
    }
    
    /**
     * Method to check the method IsVegan
     * if value is true
     */
    @Test
    public void testIsVegan()
    {
        VegetarianSandwich firstSandw = new VegetarianSandwich();
        assertTrue(firstSandw.isVegan());
    }

    /**
     * Method to check the method IsVegan
     * if value is false
     */
    @Test
    public void testIsVegan2()
    {
        VegetarianSandwich firstSandw = new VegetarianSandwich();
        firstSandw.addFilling("cheese");
        assertFalse(firstSandw.isVegan());
    }

}

