package lab7;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * This class serves to test the methods in the bagel sandwich class
 * @author Isa Melendez
 * @date 10/18/2023
 */
public class BagelSandwichTest 
{
    /**
     * Tests the contstructor of the class
     */
    @Test
    public void testConstructor(){
        BagelSandwich bagel = new BagelSandwich();
    }

    /**
     * Tests the getFillings method
     */
    @Test
    public void testGetFilling(){
        BagelSandwich bagel = new BagelSandwich();
        assertEquals("",bagel.getFilling());
    }

    /**
     * Tests that the addFilling method works correctly
     */
    @Test
    public void testAddFilling(){
        BagelSandwich bagel = new BagelSandwich();
        bagel.addFilling("cheese");
        assertEquals(" cheese",bagel.getFilling());
    }

    /**
     * Tests that the isVegetarian method throws an exception when meat is added
     */
    @Test
    public void testIsVegetarian(){
        try{
            BagelSandwich bagel = new BagelSandwich();
            bagel.isVegetarian();
            fail("Should throw an exception!");
        }
        catch(UnsupportedOperationException e){}
    }
}
